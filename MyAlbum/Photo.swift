//
//  Photo.swift
//  MyAlbum
//
//  Created by Matthew An on 8/02/17.
//  Copyright © 2017 Matthew. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class Photo {
    var albumId:Int
    var photoId:Int
    var thumbnailUrl:String
    var title:String
    var url:String
    
    init?(json:JSON?) {
        guard let albumId = json?["albumId"].int,
            let photoId = json?["id"].int,
            let thumbnailUrl = json?["thumbnailUrl"].string,
            let title = json?["title"].string,
            let url = json?["url"].string else {
                return nil
        }
        self.albumId = albumId
        self.photoId = photoId
        self.thumbnailUrl = thumbnailUrl
        self.title = title
        self.url = url
    }
}

extension Photo {
    class func fetchPhotos(inAlbum albumId:Int, completion:@escaping (([Photo]?)->Void)) -> Void {
        Alamofire.request("http://jsonplaceholder.typicode.com/photos?albumId=\(albumId)", method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                guard let photoJSONs = JSON(value).array else {
                    completion(nil)
                    return
                }
                
                var photos = [Photo]()
                for photoJSON in photoJSONs {
                    if let photo = Photo(json: photoJSON) {
                        photos.append(photo)
                    }
                }
                completion(photos)
            case .failure(_):
                completion(nil)
            }
        }
    }
}
