//
//  Album.swift
//  MyAlbum
//
//  Created by Matthew An on 8/02/17.
//  Copyright © 2017 Matthew. All rights reserved.
//

import Foundation
import SwiftyJSON

class Album {
    
    var albumId:Int
    var title:String
    var userId:Int
    
    init?(json:JSON?) {
        guard let albumId = json?["id"].int,
            let userId = json?["userId"].int,
            let title = json?["title"].string else {
            return nil
        }
        self.albumId = albumId
        self.userId = userId
        self.title = title
    }
}

extension Album {
    class func albums(json:JSON) -> [Album]? {
        guard let albumJSONs = json.array else {
            return nil
        }
        
        var albums = [Album]()
        for albumJSON in albumJSONs {
            if let album = Album(json: albumJSON) {
                albums.append(album)
            }
        }
        return albums
    }
}
