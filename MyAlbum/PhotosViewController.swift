//
//  ViewController.swift
//  MyAlbum
//
//  Created by Matthew An on 8/02/17.
//  Copyright © 2017 Matthew. All rights reserved.
//

import UIKit

class PhotosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var fullImageView: UIImageView!
    @IBOutlet weak var coverView: UIView!
    
    private var photos:[Photo]! = [] {
        willSet {
            if newValue == nil {
                self.photos = []
            }
        }
        didSet {
            assert(Thread.isMainThread)
            self.tableView.reloadData()
        }
    }
    
    var album:Album! {
        didSet {
            navigationItem.title = self.album.title
            
            Photo.fetchPhotos(inAlbum: self.album.albumId) { (photos) in
                self.photos = photos
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ThumbnailCell.thumbnailCellNib, forCellReuseIdentifier: ThumbnailCell.thumbnailCellID)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let tmp = Double(self.photos.count) / Double(ThumbnailCell.kNumOfPhotosInCell);
        let ret = ceil(tmp);
        return Int(ret);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ThumbnailCell.thumbnailCellID, for: indexPath as IndexPath) as! ThumbnailCell
        
        let idx = indexPath.row * ThumbnailCell.kNumOfPhotosInCell;
        let cellPhotos = Array(photos[idx ..< idx + min(ThumbnailCell.kNumOfPhotosInCell, photos.count - idx)]);
        cell.photos = cellPhotos;
        
        cell.pressHandler = {
            [unowned weakSelf = self] (photo: Photo) -> Void in
            weakSelf.coverView.isHidden = false;
            weakSelf.fullImageView.sd_setImage(with: URL(string: photo.url))
        };
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // MARK: - User Interface Actions
    
    @IBAction func didPressFullImageView(_ sender: Any) {
        coverView.isHidden = true
    }

}

