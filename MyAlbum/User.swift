//
//  User.swift
//  MyAlbum
//
//  Created by Matthew An on 8/02/17.
//  Copyright © 2017 Matthew. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class User {
    var userId:Int
    var name:String
    
    init?(json:JSON?) {
        guard let userId = json?["id"].int,
            let name = json?["name"].string else {
                return nil
        }
        self.userId = userId
        self.name = name
    }
}

extension User {
    class func fetch(userId:Int, completion:@escaping ((User?) -> Void)) -> Void {
        Alamofire.request("https://jsonplaceholder.typicode.com/users?id=\(userId)", method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value).array?.first
                completion(User(json: json))
            case .failure(_):
                completion(nil)
            }
        }
    }
}
