//
//  ThumbnailCell.swift
//  MyAlbum
//
//  Created by Matthew An on 8/02/17.
//  Copyright © 2017 Matthew. All rights reserved.
//

import UIKit
import SDWebImage

class ThumbnailCell: UITableViewCell {
    static let kNumOfPhotosInCell = 4
    
    static let thumbnailCellNib:UINib = UINib(nibName:"ThumbnailCell", bundle: nil)
    static let thumbnailCellID = "ThumbnailCell"

    @IBOutlet var thumbnailButtons: [UIButton]!
    var photos:[Photo] = [] {
        didSet {
            for (idx, button) in self.thumbnailButtons.enumerated() {
                if self.photos.count > idx {
                    button.sd_setImage(with: URL(string: self.photos[idx].thumbnailUrl), for: .normal)
                } else {
                    button.setImage(nil, for: .normal)
                }
            }
        }
    }
    
    var pressHandler:((Photo) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didPressThumbnailButton(_ sender: UIButton) {
        guard let idx = thumbnailButtons.index(of: sender) else {
            return
        }
        
        if photos.count > idx {
            pressHandler?(photos[idx])
        }
    }
}
