//
//  SimpleSpec.swift
//  MyAlbum
//
//  Created by Matthew An on 8/02/17.
//  Copyright © 2017 Matthew. All rights reserved.
//

import Quick
import Nimble
import SwiftyJSON
@testable import MyAlbum

class SimpleSpec: QuickSpec {
    override func spec() {
        describe("Album") {
            context("when it is created from a valid JSON object") {
                let json:JSON = [
                    "userId": 10,
                    "id": 95,
                    "title": "consectetur vel rerum qui aperiam modi eos aspernatur ipsa"
                ]
                let album = Album(json:json)
                it("should match the value") {
                    expect(album?.albumId) == 95
                    expect(album?.userId) == 10
                    expect(album?.title) == "consectetur vel rerum qui aperiam modi eos aspernatur ipsa"
                }
            }
            
            context("when it is created from an invalid JSON object") {
                let json:JSON = [
                    "userId": 10,
                    "title": "consectetur vel rerum qui aperiam modi eos aspernatur ipsa"
                ]
                let album = Album(json:json)
                it("should be nil") {
                    expect(album).to(beNil())
                }
            }
            
            context("when created from an array of JSON objects") {
                let json:JSON = [[
                    "userId": 10,
                    "id": 95,
                    "title": "consectetur vel rerum qui aperiam modi eos aspernatur ipsa"
                    ], [
                        "userId": 10,
                        "id": 96,
                        "title": "consectetur vel rerum qui aperiam modi eos aspernatur ipsa"
                    ], [
                        "id": 96,
                        "title": "consectetur vel rerum qui aperiam modi eos aspernatur ipsa"
                    ]]
                
                let albums = Album.albums(json: json)!
                
                it("should have 2 elements") {
                    expect(albums).to(haveCount(2))
                }
                
                it("all the elements should be of type Album") {
                    for album in albums {
                        expect(album).to(beAKindOf(Album.self))
                    }
                }
                
                it("the first album should match the value ") {
                    expect(albums[0].albumId) == 95
                    expect(albums[0].userId) == 10
                    expect(albums[0].title) == "consectetur vel rerum qui aperiam modi eos aspernatur ipsa"
                }
            }
        }
        
        describe("User") {
            context("when it is created from a valid JSON object") {
                let json:JSON = [
                    "id": 2,
                    "name": "Ervin Howell",
                    "username": "Antonette",
                    "email": "Shanna@melissa.tv",
                    "address": [
                        "street": "Victor Plains",
                        "suite": "Suite 879",
                        "city": "Wisokyburgh",
                        "zipcode": "90566-7771",
                        "geo": [
                            "lat": "-43.9509",
                            "lng": "-34.4618"
                        ]
                    ],
                    "phone": "010-692-6593 x09125",
                    "website": "anastasia.net",
                    "company": [
                        "name": "Deckow-Crist",
                        "catchPhrase": "Proactive didactic contingency",
                        "bs": "synergize scalable supply-chains"
                    ]
                ]
                let user = User(json:json)!
                it("should match the value") {
                    expect(user.userId) == 2
                    expect(user.name) == "Ervin Howell"
                }
            }
        }
        
        describe("Photo") {
            context("when it is created from a valid JSON object") {
                let json:JSON = [
                    "albumId": 3,
                    "id": 144,
                    "title": "aspernatur possimus consectetur in tempore distinctio a ipsa officiis",
                    "url": "http://placehold.it/600/66a752",
                    "thumbnailUrl": "http://placehold.it/150/3ad852"
                ]
                let photo = Photo(json:json)!
                it("should match the value") {
                    expect(photo.photoId) == 144
                    expect(photo.albumId) == 3
                }
            }
        }
    }

}
